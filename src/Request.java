class Request {
    int video;
    int endpoint;
    int st_requestov;

    public Request(int video, int endpoint, int st_requestov) {
        this.video = video;
        this.endpoint = endpoint;
        this.st_requestov = st_requestov;
    }

    @Override
    public String toString() {
        return "Request{" +
                "video=" + video +
                ", endpoint=" + endpoint +
                ", st_requestov=" + st_requestov +
                '}';
    }
}
