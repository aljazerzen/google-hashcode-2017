import java.util.*;

class Cache {
    HashMap<Integer, VideoPoints> pointsByVideoId = new HashMap<>();

    private static int calcPointsForRequest(Request request, int latencyToThisCache) {
        return request.st_requestov * (Main.endpoints[request.endpoint].latency - latencyToThisCache) / Main.velikosti_videov[request.video];
    }

    void dodajRequest(int request_id, int latency) {
        Request request = Main.requests[request_id];
        VideoPoints vp = pointsByVideoId.get(request.video);
        if (vp == null) {
            vp = new VideoPoints();
            pointsByVideoId.put(request.video, vp);
        }

        vp.seznam_requestov.add(request_id);
        vp.tocke += calcPointsForRequest(request, latency);
    }

    void odstraniRequest(int request_id, int latency) {
        Request request = Main.requests[request_id];
        VideoPoints vp = pointsByVideoId.get(request.video);

        vp.seznam_requestov.remove((Integer)request_id);
        vp.tocke -= calcPointsForRequest(request, latency);
    }

    List<Map.Entry<Integer, VideoPoints>> sortiraniList() {

        List<Map.Entry<Integer, VideoPoints>> list = new ArrayList<>();
        list.addAll(pointsByVideoId.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer, VideoPoints>>() {
            @Override
            public int compare(Map.Entry<Integer, VideoPoints> o1, Map.Entry<Integer, VideoPoints> o2) {
                if(o1.getValue().tocke == o2.getValue().tocke)
                    return 0;
                return o1.getValue().tocke > o2.getValue().tocke ? -1 : 1;
            }
        });

        return list;
    }
}
