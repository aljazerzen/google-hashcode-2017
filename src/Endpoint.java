import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

class Endpoint {
    int latency;
    HashMap<Integer, Integer> povezave = new HashMap<>();


    public Endpoint(Scanner sc) {
        this.latency = sc.nextInt();
        int st_povezav = sc.nextInt();

        for (int i = 0; i < st_povezav; i++)
            povezave.put(sc.nextInt(), sc.nextInt());
    }

    @Override
    public String toString() {
        return "Endpoint{" +
                "latency=" + latency +
                ", povezave=" + povezave +
                '}';
    }
}
