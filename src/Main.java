import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    static int st_videov;
    static int st_endpoints;
    static int st_requests;
    static int st_serverjev;
    static int velikost_serverja;
    static int[] velikosti_videov;
    static Endpoint[] endpoints;
    static Cache[] serverji;
    static Request[] requests;

    public static void main(String[] args) {
        read();

        for (int request_id = 0; request_id < st_requests; request_id++) {
//            System.out.println((double) request_id / st_requests);
            Request request = requests[request_id];

            for(Map.Entry<Integer, Integer> povezava : endpoints[request.endpoint].povezave.entrySet()) {
                serverji[povezava.getKey()].dodajRequest(request_id, povezava.getValue());
            }

        }

        ArrayList<ArrayList<Integer>> output = new ArrayList<>();
        ArrayList<Integer> out2 = new ArrayList<>();

        for (int server_id = 0; server_id < st_serverjev; server_id++) {
//            System.out.println((double) server_id / st_serverjev);

            List<Map.Entry<Integer, VideoPoints>> list = serverji[server_id].sortiraniList();

            int storage = 0;

            ArrayList<Integer> vidList = new ArrayList<>();

            for (Map.Entry<Integer, VideoPoints> pointsEntry : list) {

                if( velikosti_videov[pointsEntry.getKey()] + storage <= velikost_serverja) {
                    storage += velikosti_videov[pointsEntry.getKey()];
                    vidList.add(pointsEntry.getKey());

                    int size = pointsEntry.getValue().seznam_requestov.size();
                    for (int j = 0; j < size; j++) {
                        int request_id = pointsEntry.getValue().seznam_requestov.get(j);
                        Request request = requests[request_id];

                        for(Map.Entry<Integer, Integer> povezava : endpoints[request.endpoint].povezave.entrySet()) {

                            if(povezava.getKey() != server_id)
                                serverji[povezava.getKey()].odstraniRequest(request_id, povezava.getValue());
                        }
                    }
                }
            }

            if(!vidList.isEmpty()) {
                output.add(vidList);
                out2.add(server_id);
            }
        }

        System.out.println(output.size());
        for (int i = 0; i < output.size(); i++) {
            System.out.print(out2.get(i));
            for (int j = 0; j < output.get(i).size(); j++) {
                System.out.printf(" %d", output.get(i).get(j));
            }
            System.out.println();
        }
    }

    private static void read() {
        Scanner sc;
//        try {
//            sc = new Scanner(new File("./me_at_the_zoo.in"));
//        } catch (FileNotFoundException e) {
//            System.out.println(e);
            sc = new Scanner(System.in);
//        }

        st_videov = sc.nextInt();
        st_endpoints = sc.nextInt();
        st_requests = sc.nextInt();
        st_serverjev = sc.nextInt();
        velikost_serverja = sc.nextInt();

        serverji = new Cache[st_serverjev];
        for (int i = 0; i < st_serverjev; i++)
            serverji[i] = new Cache();

        velikosti_videov = new int[st_videov];
        for (int i = 0; i < st_videov; i++)
            velikosti_videov[i] = sc.nextInt();

        endpoints = new Endpoint[st_endpoints];
        for (int i = 0; i < st_endpoints; i++)
            endpoints[i] = new Endpoint(sc);

        requests = new Request[st_requests];
        for (int i = 0; i < st_requests; i++)
            requests[i] = new Request(sc.nextInt(), sc.nextInt(), sc.nextInt());
    }
}
